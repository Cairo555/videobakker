# Variable defintion
[string]$ApplicationName         = "VideoBakker"
[string]$ApplicationPath         = "C:\Program Files\$ApplicationName"
[string]$ApplicationVersion      = "0.9.1"
$Icon                            = [system.drawing.icon]::ExtractAssociatedIcon("$ApplicationPath\video.ico")

# Check if Application folder already exists in users appdata\roaming



if (($env:appdata + "\$ApplicationName"))
    {
        mkdir ($env:appdata + "\$ApplicationName")
    }
[string]$ApplicationPersonalPath = "$env:appdata\$ApplicationName"


# Check if ffmpeg.exe exists at the right location.
if (!(get-childitem -Path (".\ffmpeg.exe")))
    {
        [System.Windows.Forms.MessageBox]::Show("Er is iets fout gegaan, het lijkt er op dat ffmpeg.exe niet in dezelfde folder staat als $ApplicationName." , "$ApplicationName $ApplicationVersion") 
        exit;
    }
else { 
        [string]$FFMPEGLocation          = $PSScriptRoot + "\ffmpeg.exe"
     }


#######################################################################
#                                    REG CONFIG                       #
#######################################################################
$RegPropertys = @{
                    "HardwareRender"        = "0"; # Hardware rendering enabled = 0 ; Hardware rendering disabled = 1
                    "PreferredFileLocation" = ""  # The preferred file location for the source files. This is also the destination for the files.
                 } 


# If the registry key does not exists for this application, it will be created.
if (!(test-path -Path HKCU:\Software\$ApplicationName))
    {
        # Registry key does not exist. Therefore we create it.
        try {
                new-item -Path HKCU:\Software -Name "$ApplicationName"

                # If the property's do not exist for this application they will be created, based on the keys and values in $RegPropertys.
                foreach ($Property in $RegPropertys.Keys)
                    {
                        if (!(test-path -Path HKCU:\Software\$ApplicationName\$Property))
                            {
                                write-host $Property
                                Set-ItemProperty -Path HKCU:\Software\$ApplicationName -Name "$Property" -Value "$($RegPropertys["$Property"])"
                            }
                    }
            }
        Catch
            {
                Write-Error "Het is niet gelukt om de registry key aan te maken."
            }
    }

Function Get-Config()
    {
        $i = Get-ItemProperty -Path HKCU:\Software\$ApplicationName | Select-Object -Property PreferredFileLocation,HardwareRender,Logging
        return $i
    }

# Put the config in a variable.
$Config = Get-Config

######################################################
#            S T A R T - M A I N - C O D E           #
######################################################

######################################################

Add-Type -AssemblyName System.Windows.Forms

$FFMPEG_MP4_Converter                   = New-Object system.Windows.Forms.Form
[System.Windows.Forms.Application]::EnableVisualStyles()
$FFMPEG_MP4_Converter.Text = "$ApplicationName v$ApplicationVersion"
$FFMPEG_MP4_Converter.FormBorderStyle  = 'FixedSingle'
$FFMPEG_MP4_Converter.MaximizeBox      = $false
$FFMPEG_MP4_Converter.MinimizeBox      = $false
$FFMPEG_MP4_Converter.TopMost          = $true
$FFMPEG_MP4_Converter.Width            = 375
$FFMPEG_MP4_Converter.Height           = 220
$FFMPEG_MP4_Converter.Icon             = $Icon
$FFMPEG_MP4_Converter.ShowInTaskbar    = $True

$btnConvert        = New-Object system.windows.Forms.Button
$btnConvert.Text   = "Begin bakken"
$btnConvert.Width  = 340
$btnConvert.Height = 35
$btnConvert.Add_Click({
if(Test-Path $txtVideo.Text)
    {     
        $lblStatus.Text = "Status: Video aan het bakken..."
        ### $txtVideo.Text -replace '[_\.()\sa-z:\\-]',''
        #$Numbers = "$txtVideo.Text" -replace '[_\.\sa-z:\\-]',''
        #$Inputfile = $txtVideo.text -replace "$Numbers","%03d"
        $Inputfile = $txtVideo.text -replace "001","%03d"

        ##[System.Windows.Forms.MessageBox]::Show("$Numbers" , "$ApplicationName")
        ##[System.Windows.Forms.MessageBox]::Show("$Inputfile" , "$ApplicationName")

        $convertedVideoPath = $txtVideo.Text.Substring(0, $txtVideo.Text.LastIndexOf('.')) + "_" + $(get-date -f dd-MM-yyyy-hh-mm-ss) + ".avi"

        $Config = Get-Config

        if ($Config)
            {
                if($config.HardwareRender -eq 1)
                    {
                        # Software Render
                        Start-Process (.\ffmpeg.exe -hide_banner -i "$Inputfile" -loglevel repeat+level -c:v libx264 -profile high444 -pixel_format yuv420p -preset fast "$ConvertedVideoPath" 2> ("$ApplicationPersonalPath\$applicationName" + "-" + $(get-date -f dd-MM-yyyy-hh-mm-ss) + ".log")) -Wait
                        if (test-path -Path $ConvertedVideoPath) { [System.Windows.Forms.MessageBox]::Show("Het videobestand is aangemaakt, op deze locatie: $convertedVideoPath." , "$ApplicationName") }
                        ##Start-Process (.\ffmpeg.exe -i "$Inputfile" -c:v libx264 -profile high444 -pixel_format yuv420p -preset fast "$ConvertedVideoPath") -Wait
                    }
                elseif ($config.HardwareRender -eq 0)
                    {
                        # Hardware Render
                        Start-Process (.\ffmpeg.exe -hide_banner -hwaccel cuda -i "$Inputfile" -loglevel repeat+level -codec:v h264_nvenc -preset slow -pix_fmt yuv420p "$ConvertedVideoPath" 2> ("$ApplicationPersonalPath\$applicationName" + "-" + $(get-date -f dd-MM-yyyy-hh-mm-ss) + ".log")) -Wait
                        if (test-path -Path $ConvertedVideoPath) { [System.Windows.Forms.MessageBox]::Show("Het videobestand is aangemaakt, op deze locatie: $convertedVideoPath." , "$ApplicationName") }
                        #Start-Process (.\ffmpeg.exe -hide_banner -hwaccel cuda -i "$Inputfile" -codec:v h264_nvenc -qp 30 -crf 30 -pix_fmt yuv420p "$ConvertedVideoPath") -Wait
                    }
                else {
                        [System.Windows.Forms.MessageBox]::Show("Er is iets fout gegaan, het lijkt er op dat de config niet goed is." , "$applicationName") 
                     }
            }
    
                }
                else 
                {
                    [System.Windows.Forms.MessageBox]::Show("Bestanden zijn niet gevonden of geselecteerd!" , "$applicationName") 
                }
    $lblStatus.Text = "Status: Standby"
})

$btnConvert.location = new-object system.drawing.point(15,65)
$btnConvert.Font = "Segoe UI,10"
$FFMPEG_MP4_Converter.controls.Add($btnConvert)

$txtVideo = New-Object system.windows.Forms.TextBox
$txtVideo.Width = 227
$txtVideo.Height = 20
$txtVideo.location = new-object system.drawing.point(15,28)
$txtVideo.Font = "Segoe UI,10"
$FFMPEG_MP4_Converter.controls.Add($txtVideo)

$btnConvert.Enabled = $False

$btnBrowse = New-Object system.windows.Forms.Button
$btnBrowse.Text = "Bladeren"
$btnBrowse.Width = 105
$btnBrowse.Height = 26
try {
$btnBrowse.Add_Click({
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.CheckFileExists = $true
    $OpenFileDialog.ShowDialog() | Out-Null

    if ($OpenFileDialog.CheckFileExists -eq $true)
        {
            $btnConvert.Enabled = $True
        }
     else { $btnConvert.Enabled = $False }

    # Write the location and filename to the users registry.
    Set-ItemProperty -Path HKCU:\Software\$ApplicationName -Name "PreferredFileLocation" -Value $OpenFileDialog.FileName
    
    if ($OpenFileDialog.FileName -match "Client")
        {
            $lblStatus.text = "Open bestanden van netwerk schijf."
            $lblStatus2.text = "Het is niet toegestaan om bestanden vanaf een lokale schijf te renderen."
            $btnConvert.Enabled = $False
        }   
    elseif ($OpenFileDialog.FileName.Substring(($OpenFileDialog.FileName.Length-4),4) -eq ".png")
        {
            $txtVideo.BackColor = "#EEFFEE"
            $txtVideo.Text = $OpenFileDialog.FileName
            
            if ($txtVideo.Text -like 'Client') { $lblStatus.Text = "PROBLEEM." }  else {
            $lblStatus.Text = "Bestand, OK." }
            $checkHardWare.Checked = $True
            $lblStatus2.text = "Render tijd is optimaal."
            $lblStatus2.Visible = $True
        }        
    
    else
        {
               $txtVideo.BackColor = "#FFCCCC"
               $txtVideo.Text = $OpenFileDialog.FileName
               $lblStatus.Text = "Bestand, ondersteund geen hardware."
               $checkHardWare.Checked = $False
               #$checkHardWare.Enabled = $False
               
               $lblStatus2.text = "Het renderen zal zonder hardware langer duren."
               $lblStatus2.Visible = $True
        }

})

}
catch {  [System.Windows.Forms.MessageBox]::Show("Kan applicatie niet starten." , "$ApplicationName")  } 

$btnBrowse.location = new-object system.drawing.point(250,28)
$btnBrowse.Font = "Segoe UI,10"
$FFMPEG_MP4_Converter.controls.Add($btnBrowse)

$lblStatus = New-Object system.windows.Forms.Label
$lblStatus.Text = "Status: Standby"
$lblStatus.AutoSize = $true
$lblStatus.Width = 25
$lblStatus.Height = 10
$lblStatus.location = new-object system.drawing.point(15,130)
$lblStatus.Font = "Segoe UI,12"
$FFMPEG_MP4_Converter.controls.Add($lblStatus)

$lblStatus2 = New-Object system.windows.Forms.Label
$lblStatus2.Text = ""
$lblStatus2.AutoSize = $true
$lblStatus2.Width = 25
$lblStatus2.Height = 10
$lblStatus2.location = new-object system.drawing.point(15,155)
$lblStatus2.Font = "Segoe UI,8"
$lblStatus2.Visible = $False
$FFMPEG_MP4_Converter.controls.Add($lblStatus2)


$checkHardWare = New-Object system.windows.Forms.CheckBox
$checkHardWare.Text = "Hardware Encoding"
$checkHardWare.Height = 30
$checkHardWare.Width  = 300

if ($Config.HardwareRender -eq 0)
    {
        $checkHardWare.Checked = $True
    }
else
    {
        $checkHardWare.Checked = $False
    }

$checkHardWare.Add_Click({
if($checkHardWare.CheckState -eq "Checked")
    {
        ##[string]$Config_HardwareRendering = "Hardware = Enabled"
        Set-ItemProperty -Path HKCU:\Software\$ApplicationName -Name "HardwareRender" -Value "0"
    }
else
    {
        ##[string]$Config_HardwareRendering = "Hardware = Disabled"
        Set-ItemProperty -Path HKCU:\Software\$ApplicationName -Name "HardwareRender" -Value "1"
    }

        ##$Config_HardwareRendering | out-file $ApplicationPersonalPath\config.ini -Encoding utf8 -Force
        ##$config = Get-Content -Path $ApplicationPersonalPath\config.ini

        #Set-ItemProperty -Path HKCU:\Software\$ApplicationName -Name "HardwareRender" -Value "0"

    })

$checkHardWare.Location = new-object System.Drawing.Point(15,100)
$FFMPEG_MP4_Converter.Controls.Add($checkHardWare)


$lblVideo = New-Object system.windows.Forms.Label
$lblVideo.Text = "Bron bestanden:"
$lblVideo.AutoSize = $true
$lblVideo.Width = 25
$lblVideo.Height = 15
$lblVideo.location = new-object system.drawing.point(15,8)
$lblVideo.Font = "Segoe UI,10"
$FFMPEG_MP4_Converter.controls.Add($lblVideo)


[void]$FFMPEG_MP4_Converter.ShowDialog()

$FFMPEG_MP4_Converter.Dispose()